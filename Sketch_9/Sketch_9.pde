int iheight = 500;
float clow = 4;
float chigh = 14;
//color palette used
color[] colors = {
  color(0,0,0,50),
  color(50,50,50,50),
  color(167,167,167,255),
  color(255,255,255,0)
}; 

PImage input, output, depth, nmap, background;  

PImage selectChannel(PImage img, int no) {

  PImage res = createImage(img.width, img.height, RGB); 
  for (int y = 0; y < img.height; ++y) 
    for (int x = 0; x < img.width; ++x) {
        float r = red(img.pixels[x+y*img.width]);
        float g = green(img.pixels[x+y*img.width]);
        float b = blue(img.pixels[x+y*img.width]);
        switch (no) {
          case 0: res.pixels[x+y*img.width] = color(r);
                  break;
          case 1: res.pixels[x+y*img.width] = color(g);
                  break;
          case 2: res.pixels[x+y*img.width] = color(b);
                  break;
        }
  }  
  return res;
}

color quantizeColor(color c) {
  // TODO: Quantize the given color based on brightness 
  // HINT: have four colors and map more colors to dark then bright.
  // HINT: Use most of your color space for black and grey, very little for white.
  float bright = brightness(c);
  
  
  //COMMENT WHEN TRYING BLEND MODE, REMOVES OUTSIDE WHITE COLOR ALPHA CHANNEL
  /*if(bright==255){
    return color(255,255,255,0);
  }*/
  
  if(bright <= 100){
    return colors[0];
  }else if(bright <= 150){
    return colors[1];
  }else if(bright <= 210){
    return colors[2];
  }else{
    return colors[3];
  }
}

PImage toonShade(PImage img, PImage depth, PImage nmap) {
  
  // TODO:
  // 1) Create a result image
  // 2) Quantize the input image to a temporary image
  // 3) Blend the temporary image onto the background using PImage.blend(...) with mode BLEND
  // 4) Generate an outline from the depth image and use PImage.filter(ERODE) to thicken it
  // 5) Determine the normal discontinuities as in the previous sketch and paint them onto the image.
  // 6) Paint the lines onto the blended image from step 3 by iterating over the image and checking
  //    for black pixels.
  
  PImage resultImage = new PImage(img.width, img.height);
  PImage temp = createImage(img.width, img.height, RGB);
  //PImage temp = new PImage(img.width, img.height);
  
  //quantization
  for(int i = 0; i < img.height; i++){
    for(int j = 0; j < img.width; j++){
      int index = i * img.width + j;
      
      temp.pixels[index] = quantizeColor(img.pixels[index]);
      
    }
  }  
  //temp.save("test.png");
  resultImage.blend(background, 0, 0, background.width, background.height, 0, 0, resultImage.width, resultImage.height, BLEND);
  resultImage.blend(temp, 0, 0, temp.width, temp.height, 0, 0, resultImage.width, resultImage.height, BLEND); //CHANGE TO BLEND MODE, COMMENT IN QUANTIZE COLOR THE ALPHA CHANNEL 
  
  //depth discontinuities
  PImage depthDisc = createEdgesCanny(depth, clow, chigh);
  depthDisc.filter(ERODE);
  
  //nmap discontinuities
  PImage red = createEdgesCanny(selectChannel(nmap,0), clow, chigh);
  PImage green = createEdgesCanny(selectChannel(nmap,1), clow, chigh);
  PImage blue = createEdgesCanny(selectChannel(nmap,2), clow, chigh);  
  depthDisc.blend(red, 0, 0, img.width, img.height, 0, 0, img.width, img.height, DARKEST);
  depthDisc.blend(green, 0, 0, img.width, img.height, 0, 0, img.width, img.height, DARKEST);
  depthDisc.blend(blue, 0, 0, img.width, img.height, 0, 0, img.width, img.height, DARKEST);
    
  //line painting
  resultImage.blend(depthDisc, 0, 0, depthDisc.width, depthDisc.height, 0, 0, resultImage.width, resultImage.height, DARKEST);
   
  return resultImage; 
}

PImage createEdgesCanny(PImage img, float low, float high) {
  
  //create the detector CannyEdgeDetector 
  CannyEdgeDetector detector = new CannyEdgeDetector(); 

  //adjust its parameters as desired 
  detector.setLowThreshold(low); 
  detector.setHighThreshold(high); 

  //apply it to an image 
  detector.setSourceImage(img);
  detector.process(); 
  return detector.getEdgesImage();
}  

void setup() { 

  input = loadImage("dragon.png");
  depth = loadImage("dragon_depth.png");
  nmap = loadImage("dragon_normal.png");
  background = loadImage("background.png");
  
  input.resize(0,iheight);
  depth.resize(0,iheight);
  nmap.resize(0,iheight);

  size(500,500);
  surface.setResizable(true);
  surface.setSize(input.width, input.height);
  frameRate(3);

  output = toonShade(input,depth,nmap);  
}

void draw() {
  image(output, 0, 0); 
}

void keyPressed() {
  if (key=='1') output = input;
  if (key=='2') output = depth;
  if (key=='3') output = nmap;   
  if (key=='4') output = createEdgesCanny(input,4,14);
  if (key=='5') output = createEdgesCanny(depth,4,14);
  if (key=='6') output = toonShade(input,depth,nmap);
  
  if (key=='a') {
     chigh -= 0.2;
     output = toonShade(input,depth,nmap);
  }
  if (key=='s') {
     chigh += 0.2;
     output = toonShade(input,depth,nmap);
  }
  if (key=='q') {
     clow -= 0.1;
     output = toonShade(input,depth,nmap);
  }
  if (key=='w') {
     clow += 0.1;
     output = toonShade(input,depth,nmap);
  }
  if (key=='x') {
     save("toon.png");
  }
  println("Low: " + clow + " High: " + chigh); 
}
