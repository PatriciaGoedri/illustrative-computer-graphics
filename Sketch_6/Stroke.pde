class Stroke {
  ArrayList<PVector> pointList;
  float strokeWidth;
  color strokeColor;
  final int colorMaxDiff = 50;
  boolean expressionist = true;
  boolean colorist = false;

  // Strokes cannot be default constructed (no arguments): A position is always present!
  Stroke(PVector pp, float pwid, color pcol) {
    strokeColor = pcol; 
    strokeWidth = pwid;
    pointList = new ArrayList<PVector>();
    pointList.add(pp);
  }

  void addPoint(PVector pp) {
    pointList.add(pp);
  }

  void addPoint(float px, float py) {
    pointList.add(new PVector(px, py));
  }

  void setRadius(float pr) {
    strokeWidth = pr;
  }

  void setColor(color pcol) {
    strokeColor = pcol;
  }


  void draw() {
    stroke(strokeColor);
    strokeWeight(strokeWidth);
    // TODO; Draw all points in pointList using the line() function of processing.
    // You should connect adjacent points with a line so you get a pattern like this:
    // o---o---o---o- ... -o
    // where each "o" is a control point and --- the line between them.
    
    for(int i=0; i < pointList.size()-2; i++){
      if(colorist){
        float newRed = red(strokeColor)+random(-25,25);
        newRed = newRed > 0 && newRed < 255 ? newRed : red(strokeColor);
        float newGreen = green(strokeColor)+random(-25,25);
        newGreen = newGreen > 0 && newGreen < 255 ? newGreen : green(strokeColor);
        float newBlue = blue(strokeColor)+random(-25,25);
        newBlue = newBlue > 0 && newBlue < 255 ? newBlue : blue(strokeColor);
        
        strokeColor = color(newRed, newGreen, newBlue);
      }
    
      stroke(strokeColor);
      line(pointList.get(i).x, pointList.get(i).y, pointList.get(i+1).x, pointList.get(i+1).y);
    }
    
  }

  void movePerpendicuarToGradient(int steps, PImage inp) {
    // TODO: call growStroke exactly step times in order to enlarge the stroke.
    // If growStroke returns (-1, -1), i.e. it has found no gradient, abort the stroke.
    // Keep track of the color at the start of the stroke and if the error exceeds 
    // colorMaxDiff, also abort the stroke.
    
    // OPTIONAL: Check the angle between the last movement and the new gradient. If it
    // is less than 45° or 90° (try both) you should stop the stroke here.
    // (This was mentioned too early in the exercise, controlling the angle will
    // only be required in sketch 7.)
    float angleLimit = 90;
    PVector lastStroke = pointList.get(pointList.size() - 1);
    color startColor = inp.pixels[(int)lastStroke.x + (int)lastStroke.y * inp.width];
    if(expressionist){
      float newRed = red(startColor)+random(-25,25);
      newRed = newRed > 0 && newRed < 255 ? newRed : red(startColor);
      float newGreen = green(startColor)+random(-25,25);
      newGreen = newGreen > 0 && newGreen < 255 ? newGreen : green(startColor);
      float newBlue = blue(startColor)+random(-25,25);
      newBlue = newBlue > 0 && newBlue < 255 ? newBlue : blue(startColor);
      
      startColor = color(newRed, newGreen, newBlue);
    }
    
    setColor(startColor);
    
    for(int i=0; i < steps; i++){
      PVector returned = growStroke(inp);
      
      if (returned.x == -1 && returned.y == -1){
        break;
      }else{
        
        //removes strokes of greater than colorMaxDiff
        if(returned.x > 0 && returned.y > 0 && returned.x < inp.width && returned.y < inp.height){
          color currColor = inp.pixels[(int)returned.x + (int)returned.y * inp.width];
          float dist = dist(red(startColor), green(startColor), blue(startColor), red(currColor), green(currColor), blue(currColor));
          if(abs(dist) > colorMaxDiff){
            break;
          }
        }
        
        //removes strokes of greater than angleLimit angle
        try{
          PVector i2 = pointList.get(pointList.size()-2);
          PVector i1 = pointList.get(pointList.size()-1);
          PVector previousStroke = new PVector(i1.x-i2.x, i1.y-i2.y);
          PVector currentStroke = new PVector(returned.x-i1.x, returned.y-i1.y);
          if(degrees(PVector.angleBetween(previousStroke, currentStroke)) > angleLimit){
            break;
          }
        }catch(Exception e){}
        
        
        
        pointList.add(returned);
      }
    }  
  }


  PVector growStroke(PImage inp) {
    // TODO: Extend the stroke by figuring out where the next point shall be located
    // 1) get the last point of this stroke from pointList
    // 2) Compute the local gradient at the curent location. Implement a sobel operator for this. You can use 
    //    brightness(inp.pixels[x + y * w]) to get the brightness easily at a point x, y.
    // 3) Move orthogonally to the gradient and movy by stepSize to a new position. Add this to the point list.
    // 4) Return the location you find or (-1, -1) if you have gradient of magnitude 0. 
    PVector returnVector;
    PVector lastStroke = pointList.get(pointList.size() - 1);
    float stepSize = 10;
    
    //sobel kernels (3x3)
    float[][] kernelX = {{ -1, 0, +1}, 
                         { -2, 0, +2}, 
                         { -1, 0, +1}};
                          
    float[][] kernelY = {{ -1, -2, -1}, 
                          { 0, 0, 0}, 
                          { +1, +2, +1}};
    
    float sumX = 0;
    float sumY = 0;

    for (int ky = -1; ky <= 1; ky++) {
      for (int kx = -1; kx <= 1; kx++) { 
        try{
          float val = brightness(inp.pixels[(int)((lastStroke.x + kx) + (lastStroke.y + ky) * inp.width)]);
          sumX += kernelX[ky+1][kx+1] * val;
          sumY += kernelY[ky+1][kx+1] * val;
        }catch(Exception e){}        
      }
    }
    
    PVector normalVector = new PVector(-sumY,sumX);
    //PVector normalVector = new PVector(sumX,sumY);
    
    if(normalVector.mag()==0){
      return new PVector(-1,-1);
    }
    
    normalVector = normalVector.normalize();
    returnVector = new PVector(lastStroke.x + normalVector.x * stepSize, lastStroke.y + normalVector.y * stepSize);

    return returnVector;
  }
  
}
