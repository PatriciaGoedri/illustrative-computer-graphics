float [][] sourceArray, outputArray;
int inputHeight = 500; // What we rescale loaded images to
float clow = 2; // parameters for CannyEdge detection
float chigh = 6;
float sigma = 15;
float threshold = 1;

PImage inputImage, outputImage, depthImage, normalMapImage;
boolean colorMapping = true;

float[][] differenceImage;

/*
* Extract the selected color channel (1 = red, 2 = green, 3 = blue) from the given image.
*/
PImage selectChannel(PImage img, int channelNumber) {

  PImage res = createImage(img.width, img.height, RGB);
  img.loadPixels();

  for (int y = 0; y < img.height; ++y){ 
    for (int x = 0; x < img.width; ++x) {
      float r = red(img.pixels[x+y*img.width]);
      float g = green(img.pixels[x+y*img.width]);
      float b = blue(img.pixels[x+y*img.width]);
      switch (channelNumber) {
      case 0: 
        res.pixels[x+y*img.width] = color(r, r, r);
        break;
      case 1: 
        res.pixels[x+y*img.width] = color(g, g, g);
        break;
      case 2: 
        res.pixels[x+y*img.width] = color(b, b, b);
        break;
      }
    }
  }
  res.updatePixels();

  return res;
}

/*
* Given a depth image, return a new image with all spots where the depth value is equal
* to the target depth. The depth value in the depth image is coded as the pixel brightness.
* Found locations are marked in red.
*/
PImage markDepth(PImage depth, int targetDepth) {

  // TODO: Create a new image with the same size as depth. Then iterate depth and check the
  // brightness. If it matches the targetDepth, mark it red, otherwise put a black pixel.
  // HINT: remember PImage.loadPixels() and PImage.updatePixels().
  PImage newImage = new PImage(depth.width, depth.height);
  for(int i = 0; i < depth.height; i++){
    for(int j = 0; j < depth.width; j++){
      int index = i * depth.width + j;
      newImage.pixels[index] = brightness(depth.pixels[index]) == targetDepth ? color(255,0,0) : color(0,0,0);
    }
  }
  
  return newImage;
}

/*
* Maps the given original color to an orange/blue hue. 
*/
color colorMap(color originalColor, float thresholdBrightness)
{
  // TODO: Compare the brightness of the original color to the given threshold brightness.
  // If it's smaller, return an orange-ish hue by adding the threshold to the red and green channel.
  // Otherwise, subtract from the green and blue channel. For the green channel use half the 
  // threshold brightness.
  color returnColor;
  float r = red(originalColor);
  float g = green(originalColor);
  float b = blue(originalColor);
  
  //check if its the outside or inside
  if(brightness(originalColor) - thresholdBrightness < 170){
    if(brightness(originalColor) < thresholdBrightness){
      returnColor = color(r + thresholdBrightness, g + (thresholdBrightness/2), b);
    }else{
      returnColor = color(r - thresholdBrightness, g - (thresholdBrightness/2), b);
    }
  }else{
    returnColor = color(r, g - (thresholdBrightness/2), b - thresholdBrightness);
  }
  
  return returnColor;
}

/*
* Returns the difference of an image with its blurred version
*/
float[][] createBlurDiff(PImage depth)
{
  // TODO: Copy the depth image, and filter it using the BLUR filter with sigma as a parameter.
  // Then compute the difference between the depth image and the blurred depth image in each pixel.
  // Do not do a channel-wise difference but subtract brightnesses. Write the result to a new
  // float[][] array of appropriate size and return it.
  float[][] resultDifference = new float[depthImage.width][depthImage.height];
  PImage copy = depth.copy();
  copy.filter(BLUR, sigma);
  
  for(int i = 0; i < depth.height; i++){
    for(int j = 0; j < depth.width; j++){
      int index = i * depth.width + j;
      resultDifference[i][j] = abs(brightness(depth.pixels[index]) - brightness(copy.pixels[index]));
      //resultDifference[i][j] = brightness(copy.pixels[index]);
    }
  }

  return resultDifference;
}

/*
* Expects a difference image out of createBlurDiff and the original image
*/
PImage unsharpMask(float[][] blurredDiff, PImage originalImage, PImage depthImage) {
  // TODO: Create an empty resultImage with the same size as the depthImage.
  // Iterate the original image and modulate the color from the original image using colorMap()
  // if the flag colorMapping is true. Otherwise, subtract the difference from each channel and
  // write the result back to the image for a grey shadow effect.
  
  blurredDiff = createBlurDiff(depthImage);
  
  PImage newImage = new PImage(depthImage.width, depthImage.height);
  
  for(int i = 0; i < originalImage.height; i++){
    for(int j = 0; j < originalImage.width; j++){
      int index = i * originalImage.width + j;

      if(colorMapping){
        
        newImage.pixels[index] = colorMap(originalImage.pixels[index], blurredDiff[i][j]);
        
      }else{
        
        color pixelColor = originalImage.pixels[index];
        color depthColor = originalImage.pixels[index];
        float red = red(pixelColor) - blurredDiff[i][j];
        float green = green(pixelColor) - blurredDiff[i][j];
        float blue = blue(pixelColor) - blurredDiff[i][j];
        
        newImage.pixels[index] = color(red, green, blue);
      } 
    }
  }
  
  return newImage;
}

/*
*
*/
PImage markDiscontinuities(PImage img, PImage depth, PImage nmap) {
  
  // TODO: In this function you shall determine discontinuities in the normal and
  // depth images. 
  // 1) Use canny edge detection on the depth image with the parameters clow and chigh
  // 2) Use selectChannel on the normal map to extract the red, green and blue channel separately.
  // 3) Detect edges in the individual channel images of the previous step to find discontinuities
  // 4) Create an empty result image with the same size as the input and iterate it:
  //    * find the minimum value from the three edge detected images
  //    * Get the brightness of the depth image at the location
  //    * Mark the depth discontinuties in red and the normal discontinuities in blue.
  
  //edge images
  PImage returnImage = new PImage(img.width, img.height);
  PImage depthDisc = createEdgesCanny(depth, clow, chigh);
  PImage imgDisc = createEdgesCanny(img, clow, chigh);
  
  PImage red = createEdgesCanny(selectChannel(nmap,0), clow, chigh);
  PImage green = createEdgesCanny(selectChannel(nmap,1), clow, chigh);
  PImage blue = createEdgesCanny(selectChannel(nmap,2), clow, chigh);  
  
  //coloring loop
  for(int i = 0; i < img.height; i++){
    for(int j = 0; j < img.width; j++){
      int index = i * img.width + j;
      color value = min(red.pixels[index], green.pixels[index], blue.pixels[index]);
      color finalColor;
      
      finalColor = color(255,255,255);
      
      if(brightness(depthDisc.pixels[index]) != 255){
        finalColor = color(255,0,0);
      } 
      
      if(brightness(value) != 255){
        finalColor = color(0,0,255);
      } 
      
      if(brightness(imgDisc.pixels[index]) == 0){
        finalColor = color(0,0,0);
      }
      
      returnImage.pixels[index] = finalColor;
    }
  }
  
  return returnImage;
}

/////////////////////////////////////////////////////////////////////////////

PImage createEdgesCanny(PImage img, float low, float high) {

  //create the detector CannyEdgeDetector 
  CannyEdgeDetector detector = new CannyEdgeDetector(); 

  //adjust its parameters as desired 
  detector.setLowThreshold(low); 
  detector.setHighThreshold(high); 

  //apply it to an image 
  detector.setSourceImage(img);
  detector.process(); 
  return detector.getEdgesImage();
}

void setup() { 
  inputImage = loadImage("data/venus.png");
  inputImage.resize(0, inputHeight); // proportional scale to height

  size(500,500);
  surface.setResizable(true);
  surface.setSize(inputImage.width, inputImage.height); 
  frameRate(3);
  
  depthImage = loadImage("data/venus_depth.png");
  depthImage.resize(0, inputHeight); 

  normalMapImage = loadImage("data/venus_normal.png");
  normalMapImage.resize(0, inputHeight); 


  differenceImage = createBlurDiff(depthImage);

  outputImage = inputImage;
}


void draw() {
  image(outputImage, 0, 0);
}

void keyPressed() {
  if (key=='0') {
    outputImage = markDepth(depthImage,150);
  }
  if (key=='1') {
    outputImage = inputImage;
  }
  if (key=='2') {
    outputImage = depthImage;
  }
  if (key=='3') {
    outputImage = normalMapImage;
  }

  if (key=='4') { 
    outputImage = createEdgesCanny(inputImage, clow, chigh);
  }

  if (key=='5') { 
    outputImage = createEdgesCanny(depthImage, clow, chigh);
  }

  if (key=='6') {
    outputImage = markDiscontinuities(inputImage, depthImage, normalMapImage);
  }

  if (key=='7') {

    outputImage = unsharpMask(differenceImage, inputImage, depthImage);
  }

  if (key=='a') {
    chigh -= 0.2;
    outputImage = markDiscontinuities(inputImage, depthImage, normalMapImage);
  }
  if (key=='s') {
    chigh += 0.2;
    outputImage = markDiscontinuities(inputImage, depthImage, normalMapImage);
  }
  if (key=='q') {
    clow -= 0.1;
    outputImage = markDiscontinuities(inputImage, depthImage, normalMapImage);
  }
  if (key=='w') {
    clow += 0.1;
    outputImage = markDiscontinuities(inputImage, depthImage, normalMapImage);
  }
  if (key=='+') {

    sigma += 1; 
    differenceImage = createBlurDiff(depthImage);
    outputImage = unsharpMask(differenceImage, inputImage, depthImage);
    println("Blur sigma: " + sigma);
  }
  if (key=='-') {

    sigma -= 1; 
    differenceImage = createBlurDiff(depthImage);
    outputImage = unsharpMask(differenceImage, inputImage, depthImage);
    println("Blur sigma: " + sigma);
  }
   if (key=='m') {

    colorMapping = !colorMapping;   
    outputImage = unsharpMask(differenceImage, inputImage, depthImage);
   
  }
  if (key == 'x') {
    save("result.png");
  }
  println("Low: " + clow + " High: " + chigh);
}
