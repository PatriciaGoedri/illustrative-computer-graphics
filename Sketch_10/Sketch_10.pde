int iheight = 500; //<>//

boolean variableOutput = true;

PImage inp, outp, depth, nmap, contourImg; 

float epsilon = 0.5;

void setup() { 
  
  inp = loadImage(sketchPath("data/dragon.png"));
  depth = loadImage(sketchPath("data/dragon_depth.png"));
  nmap = loadImage(sketchPath("data/dragon_normals.png"));

  inp.resize(0, iheight); // proportional scale to height=500
  depth.resize(0, iheight); // proportional scale to height=500
  nmap.resize(0, iheight); // proportional scale to height=500

  size(10,10);
  surface.setResizable(true);
  surface.setSize(inp.width, inp.height);
  frameRate(3);
}

void draw() {
  contourImg = computeContourLines(nmap);
  image(contourImg, 0, 0);
}


PVector rgbToNormal(color c) {
  // TODO: Extract the normal vector from the given color by mapping
  // RGB to XYZ components of the vector. Since the normal can point
  // into a negative direction but color components are only in 
  // [0; 255], you should subtract from 127.
  // Normalize the vector too.
  
  float red = red(c);
  float green = green(c);
  float blue = blue(c);
  
  PVector returnVector = new PVector(127 - red, 127 - green, 127 - blue);
  returnVector.normalize();
  
  return returnVector;
}

float calcAngle(PVector v1, PVector v2) {
  float prod = PVector.dot(v1, v2);
  float mag = v1.mag() * v2.mag();
  float angle = degrees(acos(prod/mag));
  return angle;
}

PImage computeContourLines(PImage img) {
  // TODO: Create a new image, and set up a view vector (0, 0, 1). compute epsilon from the 
  // mouse position in the window, using mouseX and width. Remember that these are 
  // integers and division should yield a float! Then iterate the entire image and extract
  // the normal in each pixel. Compute the dot product with the view vector and compare
  // it to epsilon. Set the output color to white or black, depending on the result.   
  PImage newImage = new PImage(img.width, img.height);
  PVector viewVector = new PVector(0, 0, 1);
  epsilon = float(mouseX)/width;
  
  for(int x = 0; x < img.width; x++){ 
    for(int y = 0; y < img.height; y++){
      color current = img.pixels[x + y * img.width];
      PVector norm = rgbToNormal(current);
      
      float prod = PVector.dot(norm, viewVector);
      if(abs(prod)<epsilon){
        newImage.pixels[x + y * img.width] = color(0,0,0);
      }else{
        newImage.pixels[x + y * img.width] = color(255,255,255);
      }
      
      
      
    }
  }
  
  return newImage;
}
